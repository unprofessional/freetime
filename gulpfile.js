var gulp = require('gulp');
var concat = require('gulp-concat');
var vendor = require('gulp-concat-vendor');
var del = require('del');
var mbf = require('main-bower-files');

// ???: "npm install" will execute this in the 'postinstall' step

// TODO: Once everything's good, minify files

gulp.task('default', function() {
	console.log('gulpfile.js hit!');
	// QQQ: Call your tasks here?
	// AAA: Apparently running npm install will trigger all tasks..
});

gulp.task('angular', ['clean:angular'], function() {
	console.log('Concatenating/Building new gulped.js');
	gulp.src([
		'./public/js/app.js',
		'./public/js/appRoutes.js',
		'./public/js/utils/*.js',
		'./public/js/controllers/*',
		'!./public/js/controllers/*.spec.js',
		'./public/js/services/*',
		'!./public/js/services/*.spec.js',
		'./public/js/directives/*',
		'!./public/js/directives/*.spec.js'
	])
		.pipe(concat('gulped.js'))
		.pipe(gulp.dest('./public/js'));
});

gulp.task('vendor', ['clean:vendor'], function() {
	console.log('Concatenating/Building new vendor.js');
	console.log('Files to concat: ', mbf({includeDev:true}).filter(function(f){return f.substr(-2) === 'js';}));
	//gulp.src('./public/libs/*')
	gulp.src(mbf({includeDev:true}).filter(function(f){return f.substr(-2) === 'js';}))
		.pipe(vendor('vendor.js'))
		.pipe(gulp.dest('./public/js'));
});

// Cleanin' shit up
gulp.task('clean:angular', function() {
	console.log('Deleting old gulped.js');
	return del([
		'gulped.js'
	]);
});

gulp.task('clean:vendor', function() {
	console.log('Deleting old vendor.js');
	return del([
		'vendor.js'
	]);
});
