// SESSION CONFIG
var session = require('express-session');
var FileStore = require('session-file-store')(session);

var seconds = 6000;

module.exports = {
	name	:	'freetime-session-cookie-id',
	secret	:	'whatthewhat',
	saveUninitialized	:	true,
	resave	:	true,
	cookie	: { httpOnly: true, maxAge: seconds * 1000 }, // FIXME: 1 minute
	store	:	new FileStore()
}
