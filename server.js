'use strict';

var express = require('express');
var app = express();
var session = require('express-session');
var FileStore = require('session-file-store')(session);
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var methodOverride = require('method-override');
var mongoose = require('mongoose');
var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;

// EXPRESS DEBUG OUT
app.use(require('morgan')('dev'));

/** WEB CONFIG **/
var db = require('./config/db');
var monCon = mongoose.connect(db.url);
var port = process.env.PORT || 3000;

/** MIDDLEWARE **/
app.use(bodyParser.json());
app.use(bodyParser.json({type:'application/vnd.api+json'}));
app.use(bodyParser.urlencoded({extended:true}));
app.use(methodOverride('X-HTTP-Method-Override'));

// Going to '/js/*' pulls up public/js/*'
app.use(express.static(__dirname + '/public'));

// Cookie Parser (should be before session stuff)
app.use(cookieParser());

// Express sessions (declared after static resources)
var sessionConfig = require('./config/session');
app.use(session(sessionConfig));

// Passport stuff (pulls in signup and login functions)
//var User = require('./app/models/User');
var passportInit = require('./app/passport/init.js');
passportInit(passport);

// Initialize Passport
app.use(passport.initialize());
// Let Passport access sessions
app.use(passport.session());

// Node/Express Routes
require('./app/routes')(app, passport);
app.listen(port);
console.log('Magic happens on port ' + port);
