'use strict';

// NOTE: This is a child controller of "UserController"
var app = angular.module('UserUtilsController', []);

app.controller('UserUtilsController', UserUtilsController);
UserUtilsController.$inject = ['$scope', 'UserUtilsService', 'Flash'];

function UserUtilsController($scope, UserUtilsService, Flash) {

	$scope.demoFlashThing = function() {
		console.log('demoFlashThing() reached!');
		var message = '<strong>What up my glib globs!</strong>';
		var id = Flash.create('danger', message);
	};

}
