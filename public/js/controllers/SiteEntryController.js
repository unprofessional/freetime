'use strict';

var app = angular.module('SiteEntryController', ['Constants']);
app.controller('SiteEntryController', SiteEntryController);

SiteEntryController.$inject = ['$rootScope', '$scope', 'VALIDATION_STATES', 'INIT', 'UserUtilsService', 'UserService', 'FlashMessageService'];

function SiteEntryController($rootScope, $scope, VALIDATION_STATES, INIT, UserUtilsService, UserService, FlashMessageService) {

  $scope.tagline = 'Yes, this is login';
  $scope.listOfUsers = 'Array of Users';

  // TODO: Refactor these models
  $scope.userCreds = {
    email	:	null,
    password	:	null
  };

  $scope.user = {
		email : null,
    // username  : null,
    firstName : null,
    lastName  : null,
		password  : null,
		passwordConfirm	:	null // <-- obscure
	};

  $scope.demo = {
    field : null
  };

  $scope.loginBtnDisabled = function() {
    if($scope.userCreds.email == null || $scope.userCreds.password == null) return true;
    else return false;
  };

  $scope.signupBtnDisabled = function() {
    if($scope.user.email == null || $scope.user.password == null || $scope.user.passwordConfirm == null) {
      return true;
    }
    else return false;
  };

  $scope.submitLogin = function(userCreds) {
    if(!$scope.loginBtnDisabled()) return UserUtilsService.postUserLogin(userCreds);
    // else do nothing
  };

  $scope.submitSignupInfo = function(user) {

    var justTheFactsJack = {
      email : user.email,
      firstName : user.firstName,
      lastName  : user.lastName,
  		password  : user.password
    };

    if(!$scope.signupBtnDisabled()) return UserUtilsService.postUserSignup(justTheFactsJack);
  }

  // FORM VALIDATION
  $scope.validate = function() {
    $scope.isMinLengthSatisfied();
  };

  $scope.isMinLengthSatisfied = function() {
    // TODO: These broadcasts need to go to a utility method
    var length = $scope.regForm.regPass1.$viewValue.length;
    console.log('string.length! ', length);
    if(length < 8) {
      $rootScope.$broadcast(VALIDATION_STATES.PASSWORD_MINLENGTH_NOT_MET,
        'Password must be at least 8 characters long!');
    } else {
      $rootScope.$broadcast(VALIDATION_STATES.PASSWORD_VALID,
        'All good in the hood');
    }
  };

  // $scope.

  // ADMIN DEBUG
  $scope.getAllUsers = function() {
		UserService.getAll().then(function(response) {
			$scope.listOfUsers = response.data;
		});
	};

  // FLASH STUFF
  $scope.loginFlashForgotPassword = function() {
    FlashMessageService.loginFlashForgotPassword();
  };

  var init = function() {
    var messages = {
      loginMessage: '<strong>please enter your login credentials!</strong>',
      signupnMessage: '<strong>welcome! enter your info and sign up to get started!</strong>'
    };
    $rootScope.$broadcast(INIT.SITE_ENTRY, messages);
  };
  init();

}
