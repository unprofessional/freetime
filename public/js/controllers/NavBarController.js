'use strict';

var app = angular.module('NavBarController', ['Constants']);
app.controller('NavBarController', NavBarController);

NavBarController.$inject = ['$scope', '$location', 'AUTH_STATES', 'AuthService'];

function NavBarController($scope, $location, AUTH_STATES, AuthService) {

	console.log('NavBarController has been hit!');

	// Determine user status for ng-if display purposes
	$scope.isLoggedIn = AuthService.isLoggedIn();
	// $scope.user = AuthService.currentUser();

	// TODO: Throw the receivers in a service

	// On USER LOGIN success
	$scope.$on(AUTH_STATES.LOGIN_SUCCESS, function(event, msg){
		console.log('msg: ', msg);
		$scope.isLoggedIn = AuthService.isLoggedIn();
	});

	// On USER LOGOUT success
	$scope.$on(AUTH_STATES.LOGOUT_SUCCESS, function(event, msg){
		console.log('msg: ', msg);
		$scope.isLoggedIn = AuthService.isLoggedIn();
		$location.path('/login');
	});

	// Perform the logout
	$scope.logout = function() {
		console.log('NavBarController -> AuthService.logout()');
		AuthService.logout();
	};

}
