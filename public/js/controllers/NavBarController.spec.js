//'use strict';

describe('NavBar Controller', function() {
	
	var $controller,
		$scope,
		$location,
		AuthService;
		
	beforeEach(module('NavBarController'));
	
	beforeEach(function() {
		AuthService = jasmine.createSpyObj('AuthService', [
			'isLoggedIn',
			'logout'
		]);
		
		module(function($provide) {
			$provide.value('AuthService', AuthService);
		});
	});
	
	beforeEach(inject(function(_$controller_, $rootScope) {
		$scope = $rootScope.$new();
		$controller = _$controller_('NavBarController', {
			$scope: $scope
		});
	}));
	
	it('should be defined', function() {
		expect($controller).toBeDefined();
	});
	
	it('should call services', function() {
		expect(AuthService.isLoggedIn).toHaveBeenCalled();
		//expect(AuthService.logout).toHaveBeenCalled();
	});
	
	// When NavBarController.logout() is hit
	describe('$scope.logout', function() {
		it('relays logout request to AuthService logout function', function() {
			$scope.logout();
			expect(AuthService.logout).toHaveBeenCalled();
		});
	});
	
});