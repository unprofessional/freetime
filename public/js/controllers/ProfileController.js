'use strict';

var app = angular.module('ProfileController', []);

app.controller('ProfileController', ProfileController);
ProfileController.$inject = ['$scope', '$rootScope'];

function ProfileController($scope, $rootScope) {

	$scope.tagline = 'sup profile!';

	$scope.user = $rootScope.currentUser;

	$scope.updateUser = function(user) {
		
	};

	$scope.updateProfile = function(profile) {

	};

}
