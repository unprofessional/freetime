'use strict';

var app = angular.module('UserController', []);

app.controller('UserController', UserController);
UserController.$inject = ['$scope', 'UserService'];

function UserController($scope, UserService) {

	$scope.user = {
		email : null,
		password : null
		,passwordConfirm	:	null
	};

	$scope.getAllUsers = function() {
		UserService.getAll().then(function(response){
			$scope.listOfUsers = response.data;
		});
	};

	$scope.get = function(username) {
		return UserService.get(username);
	};

	$scope.post = function(userData) {
		return UserService.post(userData);
	};

	$scope.put = function(username, userData) {
		return UserService.put(username, userData);
	};

	$scope.delete = function(username) {
		return UserService.delete(username);
	};

}
