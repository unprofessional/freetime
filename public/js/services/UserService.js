'use strict';

var app = angular.module('UserService', []).factory('UserService', UserService);
UserService.$inject = ['$http'];
function UserService($http) {
	
	return {
		getAll : function() {
			return $http.get('/api/users');
		},
		get : function(username) {
			return $http.get('/api/user/' + username);
		},
		// Responsibility for hashing+salting password should go to backend
		post : function(userData) {
			console.log('UserService.post.userData: ', userData);
			return $http.post('/api/user', userData);
		},
		put : function(username, userData) {
			return $http.put('/api/user/' + username, userData);
		},
		delete : function(username) {
			return $http.delete('/api/user/' + username);
		}
	};
	
};