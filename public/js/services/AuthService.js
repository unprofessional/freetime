'use strict';

var app = angular.module('AuthService', ['Constants']).factory('AuthService', AuthService);
AuthService.$inject = [
	'$location',
	'$rootScope',
	'AUTH_STATES',
	'SessionService',
	'UserService',
	'$cookies',
	'$timeout'
];

function AuthService(
	$location,
	$rootScope,
	AUTH_STATES,
	SessionService,
	UserService,
	$cookies,
	$timeout
) {
	// console.log('--------------> $cookies.get(\'user\')(1)', $cookies.get('user'));
	$rootScope.currentUser = $cookies.get('user') || null;
	$cookies.remove('user');

	// Need to parse the user from the cookie as JSON if stringified
	if(typeof $rootScope.currentUser === 'string') {
		$rootScope.currentUser = JSON.parse($rootScope.currentUser);
	} // else do nothing

	console.log('AuthService has been hit!');
	console.log('AuthService.$rootScope.currentUser: ', $rootScope.currentUser);

	return {

		login		:	function(provider, user, callback) {
			console.log('AuthService.login().user: ', user);
			var cb = callback || angular.noop;
			SessionService.save({
				provider	:	provider,
				email			:	user.email,
				//username	:	user.username,
				password	:	user.password//,
				//rememberMe	:	user.rememberMe
			},
			function(response) {
				// console.log('AuthService: Successful call? Even if a 401 is returned?');
				// console.log('AuthService.login.response._id: ', response._id);
				console.log('AuthService.login.response: ', response);
				if(!response.error) {
					console.log('AuthService.login.response does not contain any errors!');
					$rootScope.currentUser = response;
					$rootScope.$broadcast(AUTH_STATES.LOGIN_SUCCESS, 'User is logged in now');
					$timeout(function(){
						console.log('!!!!! session timed out !!!!!');
					}, 60000);
				} else {
					console.log('AuthService.login.response contains error: ', response.error);
					// Should send the error to something. Either broadcast or use rootscope var
					// -- broadcasting seems to be the better choice here imo
					$rootScope.$broadcast(AUTH_STATES.LOGIN_FAILURE, response.error);
				}
				return cb();
			},
			function(err) {
				console.log('AuthService: Failed call? Maybe with ERR_CONNECTION_REFUSED?');
				console.log('AuthService: Failed call? err: ', err);
				if(err.status === -1) {
					$rootScope.$broadcast(AUTH_STATES.LOGIN_FAILURE, 'The connection was refused. No network connectivity?');
				}
				return cb(err.data);
			});
		},

		// This is only useful for successful-cases
		sessionExists	:	function(callback) {
			var cb = callback || angular.noop;

			/* There is no use in trying to capture any errors as a 401 will prevent any failure-case
			 * code from being executed...
			 */
			return SessionService.get(
				// Only if on successful call
				function(response) {
					console.log('AuthService.sessionExists() -> response: ', response);

					// FIXME: We will need to return a wrapped user object from the backend
					// for a more elegant solution.
					// FOR NOW....
					if(response._id) {
						$rootScope.currentUser = response;
						$rootScope.$broadcast(AUTH_STATES.LOGIN_SUCCESS, 'User is logged in now');
					}
					else {
						$rootScope.currentUser = null;
						$rootScope.$broadcast(AUTH_STATES.LOGOUT_SUCCESS, 'User is logged out now');
					}
					return cb(response);
				}
			).$promise.then(
				// Success (status ~= 200) !!!!!!!!!! This seems redundant if all it is doing
				// is echoing the results from the initial call.
				// .... investigate further ....
				function(data) {
					console.log('AuthService.sessionExists() -> data: ', data);
					return cb(data);
				},
				// Error (status >= 400) !!!!!!!!! THIS IS NO LONGER NECESSARY
				function(error) {
					if(error.status === 401) {
						console.log('AuthService.sessionExists() -> error.status === 401');
						$rootScope.currentUser = null;
						$rootScope.$broadcast(AUTH_STATES.LOGOUT_SUCCESS, 'User is logged out now');
					} else {
						console.log('AuthService.sessionExists() -> error: ', error);
					}
					return cb(error);
				}
			);
		},

		isLoggedIn	:	function() {
			/* console.log('AuthService.isLoggedIn() called!'); */
			return ($rootScope.currentUser) ? true : false;
		},

		logout		:	function(callback) {
			var cb = callback || angular.noop;
			console.log('AuthService.logout() has been hit!');
			SessionService.delete(function(response){
				console.log('AuthService.logout() -> SessionService.delete().response: ', response);
				$rootScope.currentUser = null;
				$rootScope.$broadcast(AUTH_STATES.LOGOUT_SUCCESS, 'User is logged out now');
				return cb;
			}, function(err) {
				return cb(err.data);
			});
		}

	};

}
