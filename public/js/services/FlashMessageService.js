'use strict';

/** This service intercepts any broadcasts for Flash message pop-ups **/

var app = angular.module('FlashMessageService', ['Constants']).factory('FlashMessageService', FlashMessageService);
FlashMessageService.$inject = ['$rootScope', 'AUTH_STATES', 'VALIDATION_STATES', 'INIT', 'Flash'];

function FlashMessageService($rootScope, AUTH_STATES, VALIDATION_STATES, INIT, Flash) {

  var loginFlashId = -1;
  var signupFlashId = -1;

  /** BROADCASTS **/ // !!!!!!!!!!!!!!!!!!! switch case?
  $rootScope.$on(INIT.SITE_ENTRY, function(event, messages) {
    // console.log('messages: ', messages);
    Flash.clear(); // shouldn't need to do this..
    var loginMessage = messages.loginMessage;
    var signupnMessage = messages.signupnMessage;
    loginFlashId = Flash.create('info', loginMessage, 0, {container:'login-flash', id:'login-flash'}, false);
    signupFlashId = Flash.create('info', signupnMessage, 0, {container:'signup-flash', id:'signup-flash'}, false);
  });

  $rootScope.$on(AUTH_STATES.LOGIN_FAILURE, function(event, message) {
    console.log('message: ', message);
    Flash.dismiss(loginFlashId);
    message = '<strong>' + message + '</strong>';
    loginFlashId = Flash.create('danger', message, 0, {container:'login-flash', id:'login-flash'}, false);
  });

  $rootScope.$on(VALIDATION_STATES.PASSWORD_CONF_NOT_EQUAL, function(event, message) {
    console.log('message: ', message);
    Flash.dismiss(signupFlashId);
    message = '<strong>' + message + '</strong>';
    signupFlashId = Flash.create('danger', message, 0, {container:'signup-flash', id:'signup-flash'}, false);
  });

  $rootScope.$on(VALIDATION_STATES.PASSWORD_CONF_EQUAL, function(event, message) {
    console.log('message: ', message);
    Flash.dismiss(signupFlashId);
    message = '<strong>' + message + '</strong>';
    signupFlashId = Flash.create('success', message, 0, {container:'signup-flash', id:'signup-flash'}, false);
  });

  $rootScope.$on(VALIDATION_STATES.PASSWORD_MINLENGTH_NOT_MET, function(event, message) {
    console.log('message: ', message);
    Flash.dismiss(signupFlashId);
    message = '<strong>' + message + '</strong>';
    signupFlashId = Flash.create('danger', message, 0, {container:'signup-flash', id:'signup-flash'}, false);
  });

  $rootScope.$on(VALIDATION_STATES.PASSWORD_VALID, function(event, message) {
    console.log('message: ', message);
    Flash.dismiss(signupFlashId);
    message = '<strong>' + message + '</strong>';
    signupFlashId = Flash.create('success', message, 0, {container:'signup-flash', id:'signup-flash'}, false);
  });

  /** OBJECT METHODS **/
  var service = {

    loginFlashForgotPassword: function() {
      Flash.dismiss(loginFlashId);
      var message = '<strong>what up my glib globs!</strong>';
      loginFlashId = Flash.create('danger', message, 0, {container:'login-flash', id:'login-flash'}, false);
  	},

    signupFlashThing: function() {
      Flash.dismiss(signupFlashId);
  		var message = '<strong>jhkasdfjhahskdfasds!</strong>';
  		signupFlashId = Flash.create('danger', message, 0, {container:'signup-flash', id:'signup-flash'}, false);
  	}

  };

  return service;
}
