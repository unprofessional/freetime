'use strict';

var app = angular.module('SessionService',[]).factory('SessionService', SessionService);

SessionService.$inject = ['$resource'];
function SessionService($resource) {
	
	// This requires the Session object-model approach to be implemented
	// so sessions can have their own routes
	
	// FUTURE: If we need to extend and customize the resource model, we can do this
	/* var serviceResource = $resource('/auth/session/', {}, {
		'get':	{
			method:	'GET',
			interceptor:	{
				responseError:	function() {
					console.log('SessionService GET has encountered a problem!');
				}
			}
		}
	}); */
	
	/* serviceResource.query().$promise.then(function(data){
		console.log('---------------> data: ', data);
	}, function(error){
		console.log('---------------> error: ', error);
	}); */
	
	/* return serviceResource; */
	
	return $resource('/auth/session/');
	
}