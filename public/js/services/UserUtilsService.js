'use strict'

var app = angular.module('UserUtilsService', []).factory('UserUtilsService', UserUtilsService);
UserUtilsService.$inject = ['$http','$location', 'AuthService'];

function UserUtilsService($http, $location, AuthService) {

	return {

		postUserLogin	:	function(userCreds) { // pass in $scope.user instead?
			// TODO: Find out what this API endpoint (URL) usually looks like
			console.log('UserUtilsService.postUserLogin.userCreds:',userCreds);
			return AuthService.login('password', userCreds, function(err) {
				console.log('UserUtilsService.postUserLogin -> AuthService.login -> Inside callback');
				if(!err) {
					$location.path('/profile');
				} else {
					console.log('UserUtilsService.postUserLogin -> AuthService.login -> err!!! ', err);
				}
			});
		},

		postUserSignup	:	function(user) {
			console.log('UserUtilsService.postUserSignup.user:',user);
			return $http.post('/api/user', user)
			.success(function(user) {
				console.log('UserUtilsService.postUserSignup.success! user: ', user);

				return AuthService.sessionExists(function(response) {
					console.log('UserUtilsService.postUserSignup -> AuthService.sessionExists -> Inside callback');
					if(!response.error) {
						$location.path('/profile');
					} else {
						console.log('UserUtilsService.postUserSignup -> AuthService.sessionExists -> response.error!!! ',
						response.error);
					}
				});

				// return AuthService.login('password', user, function(err) {
				// 		console.log('UserUtilsService.postUserSignup -> AuthService.login -> Inside callback');
				// 		if(!err) {
				// 			$location.path('/profile');
				// 		} else {
				// 			console.log('UserUtilsService.postUserSignup -> AuthService.login -> err!!! ', err);
				// 		}
				// });
			})
			.error(function(err){
				console.log('There was a problem: ' + err);
			});
		}

	};

}
