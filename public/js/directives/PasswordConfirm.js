'use strict';

var app = angular.module('compareTo', []);
app.directive('compareTo', compareTo);
compareTo.$inject = ['$rootScope', 'Flash'];

function compareTo($rootScope, Flash) {

  // console.log('compareTo directive reached!');

  return {
    require: 'ngModel',
    scope:  {
      otherModelValue: '=compareTo'
    },
    link: function($scope, element, attributes, ngModel) {

      // ngModel = user.password
      // otherModelValue = user.passwordConfirm
      ngModel.$validators.compareTo = function(modelValue) {
        // console.log('$scope.otherModelValue: ', $scope.otherModelValue);

        // Remember, these are attributes from the user.passwordConfirm field, NOT user.passwordConfirm
        // console.log('--->', attributes.$$element['0']);
        var indexOfNgPristine = attributes.$$element['0'].classList.value.split(' ').indexOf('ng-pristine');

        // If unequal
        if(modelValue != $scope.otherModelValue) {
          console.log('Passwords do not match!');
          $rootScope.$broadcast(Constants.VALIDATION.passwordConfirmNotEqual, 'Passwords do not match!');
        }
        // If equal
        if(modelValue == $scope.otherModelValue && indexOfNgPristine == -1) {
          console.log('Passwords match!');
          $rootScope.$broadcast(Constants.VALIDATION.passwordConfirmEqual, 'Passwords match!');
        }

        return modelValue == $scope.otherModelValue;
      };

      $scope.$watch('otherModelValue', function() {
        ngModel.$validate();
      });

    }
  };

}
