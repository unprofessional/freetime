'use strict';

angular.module('freetimeApp', [
	/** EXTERNAL DEPENDENCIES **/
	'ngCookies',
	'ngResource', // installed
	//'ngSanitize',
	'ngRoute', // installed
	'ngFlash', // installed

	/** LOCAL **/
	// ngRoutes
	'appRoutes',

	// Utilities
	'Constants',

	// Controllers
	'SiteEntryController',
	'MainController',
	'ProfileController',
	'UserController',
	'UserUtilsController',
	'NavBarController',

	// Services
	'UserService',
	'UserUtilsService',
	'AuthService',
	'SessionService',
	'FlashMessageService',

	// Directives
	'compareTo'
]);
