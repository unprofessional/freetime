'use strict';

var app = angular.module('Constants', []);
app.constant('AUTH_STATES', (function() {
  return {
    LOGIN_SUCCESS: 'auth-login-success',
    LOGIN_FAILURE: 'auth-login-failure',
    LOGOUT_SUCCESS: 'auth-logout-success'
  }
})());
app.constant('VALIDATION_STATES', (function() {
  return {
    PASSWORD_CONF_NOT_EQUAL: 'password-confirm-not-equal',
    PASSWORD_CONF_EQUAL: 'password-confirm-equal',
    PASSWORD_MINLENGTH_NOT_MET: 'password-minlength-not-met',
    PASSWORD_VALID: 'password-valid'
  }
})());
app.constant('INIT', (function() {
  return {
    SITE_ENTRY: 'init-site-entry'
  }
})());
