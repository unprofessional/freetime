'use strict';
// ANGULAR ROUTES

var app = angular.module('appRoutes', []);

app.config(function($routeProvider, $locationProvider) {

	$routeProvider
		.when('/', {
			templateUrl: 'views/default.html'
		}) // This wil crash the app (if somehow accessed directly)
		.when('/home', {
			templateUrl: 'views/home.html',
			controller: 'MainController'
		})
		// profile page
		.when('/profile', {
			templateUrl: 'views/profile.view.html',
			controller: 'ProfileController'
		})
		.when('/login', {
			templateUrl: 'views/login.html',
			controller: 'SiteEntryController'
		})
		.otherwise({
			redirectTo: '/'
		});
	$locationProvider.html5Mode({
		enabled : true,
		requireBase : false
	});
})
.run(function($rootScope, $location, AuthService) {

	var currentLocation = $location.path();

	$rootScope.$on('$routeChangeStart', function(event){

		console.log('appRoutes -> $routeChangeStart starting!');

		if($rootScope.currentUser != null) {
			console.log('appRoutes -> $rootScope.currentUser: ', $rootScope.currentUser);
			AuthService.sessionExists(sessionExistsCallback);
		} else {
			console.log('appRoutes -> $rootScope.currentUser: ', $rootScope.currentUser);
		}

		var allowedLoggedInPaths = ['/', '/logout', '/home', '/profile'];
		var allowedAnonUserPaths = ['/login', '/signup'];
		var isLoggedIn = AuthService.isLoggedIn();

		/** FINITE_STATES **/
		// POSITIVE_CASES
		var ANON_ACCESSING_ANON_PATH = !isLoggedIn && (allowedAnonUserPaths.indexOf($location.path()) != -1 );
		var LOGGEDIN_ACCESSING_LOGGEDIN_PATH = isLoggedIn && (allowedLoggedInPaths.indexOf($location.path()) != -1);
		// NEGATIVE_CASES
		var ANON_ACCESSING_LOGGEDIN_PATH = !isLoggedIn && (allowedLoggedInPaths.indexOf($location.path()) != -1 );
		var LOGGEDIN_ACCESSING_ANON_PATH = isLoggedIn && (allowedAnonUserPaths.indexOf($location.path()) != -1);

		var intendedLocation = $location.path();

		// console.log('appRoutes -> currentLocation: ', currentLocation);

		if(ANON_ACCESSING_ANON_PATH) {
			// console.log('appRoutes -> ANON GOOD, is allowed -> intendedLocation: ', intendedLocation);
		}

		// This is the path we need to check the server for a living session
		if(LOGGEDIN_ACCESSING_LOGGEDIN_PATH) {
			// console.log('appRoutes -> LOGGEDIN GOOD, is allowed -> intendedLocation: ', intendedLocation);
			if(currentLocation != intendedLocation) {
				console.log('appRoutes -> Cancelling route to check if user isLoggedIn');
				event.preventDefault();
			}
		}

		if(ANON_ACCESSING_LOGGEDIN_PATH) {
			// console.log('appRoutes -> DENIED, redirecting to /login -> intendedLocation: ', intendedLocation);
			$location.path('/login');
		}

		if(LOGGEDIN_ACCESSING_ANON_PATH) {
			// console.log('appRoutes -> ALREADY LOGGED IN, redirecting to /home -> intendedLocation: ', intendedLocation);
			$location.path('/home');
		}

		// Named callback function for session health checks against the server
		function sessionExistsCallback(data) {
			console.log('appRoutes -> sessionExistsCallback starting!');

			if(data.error) {
				console.log('appRoutes -> Response contains ERROR! data.error: ', data.error);
				isLoggedIn = false;
				$location.path('/login');
			}

			if(isLoggedIn) {
				console.log('appRoutes -> user isLoggedIn!');
				$location.path(intendedLocation); // This is the magic moment...
				currentLocation = intendedLocation;
			} else {
				console.log('appRoutes -> user isNOTLoggedIn!');
			}
			console.log('appRoutes -> sessionExistsCallback ending!');
		}

		console.log('appRoutes -> $routeChangeStart ending!');

	});

});
