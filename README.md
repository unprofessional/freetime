A MEAN-stack application currently in boiler-plate code state (manually put together by myself).

The goal is to create a secure social network with functionality similar to Facebook via MEAN-stack.

This directory-level is for the NodeJS/Express Stuff -- the models, Passport config, and sExpress routes are in /app/ while the frontend "client" code is in /public/

Ideally, the API backend and Angular frontend will be separated out into two distinct projects.

Feature and issue tracking can be found on Trello here: https://trello.com/b/nYXVUN6V/freetime
