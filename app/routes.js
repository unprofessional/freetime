'use strict';
// NODE/EXPRESS ROUTES

var User = require('./models/User');
var Session = require('./models/Session');
var path = require('path');
var passport = require('passport');
var routeAuth = require('./route-auth');

module.exports = function(app, passport) {

	// FIXME: Hmm... this will get messy real quick.  Maybe apiUserRoutes???
	// That would mean we would need to declare
	// 		require('apiUserRoutes');
	// here, before the catch-all...

	// ------> FIXME: Move to the Session object-model model
	// TODO: Create Session mongoose model

	/** SESSION API ROUTES **/
	app.get('/auth/session', routeAuth.ensureAuthenticated, Session.session);
	app.post('/auth/session', Session.login);
	app.delete('/auth/session', Session.logout);

	// TODO: This endpoint will be deprecated when Session object-model is implemented
/*	app.post('/api/login', function(req, res, next) {
		passport.authenticate('login', function(err, user, info) {
			var error = err || info;
			if(error) return res.json(400, error);
			req.logIn(user, function(err) {
				//console.log('req.logIn: ', req.logIn);
				if(err) return res.send(err);
				res.json(req.user.user_info);
			});
		})(req, res, next);
	});
*/

	// TEST
	//app.get('/profile', routeAuth.ensureAuthenticated);

	// Create a user
	app.post('/api/user', passport.authenticate('signup'), function(req, res) {
		// TODO: Create virtual info in User model object from Schema
		console.log('POST /api/user reached! req.user: ', req.user);
		var sanitizedUser = {
			id : req.user._id,
			//username : req.user.username,
			email	:	req.user.email,
			firstName	:	req.user.firstName,
			lastName	:	req.user.lastName,
			isAuthenticated : req.isAuthenticated()
		};
		res.send(sanitizedUser);
	});

	/** USER API ROUTES -- basic param format here is: (url, expectedAction) **/
	// Get ALL Users
	app.get('/api/users', User.findAll);
	// Get one User
	app.get('/api/user/:username', User.findByUsername);
	// Update a user
	app.put('/api/user/:username', User.update);
	// Delete a user
	app.delete('/api/user/:username', User.delete);

	// !!! SESSION VIEWS !!!
	app.get('*', function initViewsCount(req, res, next) {
		//console.log('initViewsCount hit');
		if(typeof req.session.views === 'undefined') {
			req.session.views = 0;
			console.log('session view was initialized');
		}
		return next();
	});
	app.get('*', function incrViewsCount(req, res, next) {
		//console.log('incrViewsCount hit');
		console.assert(typeof req.session.views === 'number',
			'missing views count in the session: ', req.session);
		req.session.views++;
		return next();
	});

	// !!! DEBUG SESSION OUT !!!
	app.use(function printSession(req, res, next) {
		console.log('----> req.session:', req.session);
		console.log('----> req.isAuthenticated():', req.isAuthenticated());
		return next();
	});

	// Catch-all must be here after API endpoint declarations
	app.get('/*', function(req, res) {
		/**
		 * FIXME: This should probably go elsewhere...
		 * The cookie can't contain a "physical" object, so it must be JSON.stringified
		 */
		if(req.user) res.cookie('user', JSON.stringify(req.user.user_info));
		//if(req.user) res.cookie('user', req.user.user_info);

		console.log('----> /* was called');
		//console.log('----> /* res.cookie: ', res.cookie);

		res.sendFile(path.resolve(__dirname, '../public/index.html'));
	});

};
