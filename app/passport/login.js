var LocalStrategy = require('passport-local').Strategy;
var User = require('../models/User');
var bcrypt = require('bcrypt-nodejs');

module.exports = function(passport){

	passport.use('login', new LocalStrategy(
		{ passReqToCallback:true, usernameField:'email' },
		function(req, username, password, done){
			console.log('passport login strategy called');
			User.findOne(
				{'email':username},
				function(err, user){
					console.log('passport -> login -> User.findOne()');
					// If error, return done method
					if(err){
						// INFO: True error... i.e. Mongoose/MongoDB problem
						console.log('err: ' + err);
						return done(err);
					}
					// If email doesn't exist
					if(!user){
						console.log('passport -> login -> User.findOne(): User does not exist!');
						return done(null, false, {error:'Username and/or password incorrect'});
					}
					if(!isValidPassword(user, password)){
						console.log('passport -> login -> User.findOne(): Incorrect password!');
						return done(null, false, {error:'Username and/or password incorrect'});
					}
					console.log('passport -> login -> User.findOne(): all good in the hood');
					// All good in the hood
					return done(null, user);
					// return done(null, user, {success:'All good in the hood'});
				}
			);
		})
	);

	var isValidPassword = function(user, password) {
		console.log('Checking bcrypt signature..');
		return bcrypt.compareSync(password, user.password);
	};

};
