var LocalStrategy = require('passport-local').Strategy;
var User = require('../models/User');
var bcrypt = require('bcrypt-nodejs');

module.exports = function(passport) {
	passport.use('signup', new LocalStrategy({
			// Allows us to pass request to callback
			passReqToCallback : true,
			usernameField:'email'
		},
		function(req, username, password, done)	{
			console.log('passport.use(signup) reached');

			findOrCreateUser = function()
			{
				//console.log('req:', req);
				console.log('signup.findOrCreateUser.email:',  req.param('email'));
				console.log('signup.findOrCreateUser.username:', username);
				console.log('signup.findOrCreateUser.password:', password);

				// Find user in Mongo with username
				User.findOne(
					{'email':username},
					function(err, user) {
						// If err, return done method
						if(err) {
							return done(err);
						}
						// If user exists
						if(user) {
							console.log('User already exists with email: ' + username);
							//return done(null, false, req.flash('message','User Already Exists!'));
							return done(null, false);
						}
						else {
							// If no user with email, create user
							var newUser = new User();
							newUser.email = req.param('email');
							//newUser.username = username;
							newUser.password = createHash(password);
							newUser.firstName = req.param('firstName');
							newUser.lastName = req.param('lastName');

							// Save user
							newUser.save(function(err) {
								if(err) {
									console.log('Error saving user: ' + err);
									throw err;
								}
								console.log();
								return done(null, newUser);
							});
						}
					}
				);
			};
			// TODO: Find out why we need to do this
			process.nextTick(findOrCreateUser);
		})
	);

	var createHash = function(password) {
		return bcrypt.hashSync(password, bcrypt.genSaltSync(10), null);
	};
};
