'use strict';

var mongoose = require('mongoose');
var passport = require('passport');

/**
 * Session (GET)
 * returns info on authenticated user
 * Note that this returns user.user_info virtual object
 */
exports.session = function (req, res) {
	console.log('Session.session hit! Sending back user info');
	// res.json(req.user.user_info);
	res.status(200).json(req.user.user_info);
};

/**
 * Logout (DELETE)
 * returns nothing
 */
exports.logout = function (req, res) {
	console.log('Session.logout hit!');
	if(req.user) {
		req.logout();
		res.sendStatus(200);
	} else {
		// res.sendStatus(400, "Not logged in");
		res.status(200).json({error:'Not logged in!'});
	}
};

/**
 *  Login (POST)
 *  requires: {email, password}
 */
exports.login = function (req, res, next) {
	console.log('Session.login hit!');
	console.log('Session.login.req.body: ', req.body);
	passport.authenticate('login', function(err, user, info) {
		var error = err || info;
		console.log('error: ', error);
		// INFO: Because login.js's login strategy ALWAYS returns a message (error or success),
		// this will ALWAYS be hit.
		if (error) {
			// return res.status(400).json(error);
			return res.status(200).json(error);
		} // else
		req.logIn(user, function(err) {
			if (err) {
				console.log('Session(Model).logIn.err: ', err);
				// return res.send(err);
				return res.json({error:'User login failed!'});
			} // else
			console.log('Session(Model).logIn.success? ', user);
			res.json(req.user.user_info);
		});
	})(req, res, next);
};
