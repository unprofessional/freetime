'use strict';

var mongoose = require('mongoose');
mongoose.set('debug', true);
var passportLocalMongoose = require('passport-local-mongoose');

// REFERTO: https://www.sitepoint.com/user-authentication-mean-stack/

var userSchema = new mongoose.Schema(
	{
		email : {type:String, unique:true, required:true, default:'default'},
    firstName : {type:String, unique:false, required:true, default:'default'},
    lastName : {type:String, unique:false, required:true, default:'default'},
		//username : {type:String, unique:true, required:true, default:'default'},
		password : {type:String, required:true, default:'default'}
	},
	/*
	 * Because Mongoose tries to be smart and pluralize singular object names
	 * and this is a behavior we want to control explicitly, we will be declaring
	 * explicit table/collection name declaration here
	 */
	{ collection : 'users' }
);

/**
 * Virtuals
 */
userSchema.virtual('user_info').get(function() {
	return {
		'_id'		:	this._id,
		'email'		:	this.email,

		//'username'	:	this.username
	};
});

var User = mongoose.model('User', userSchema);

// TODO:	Decide if this logic should go in the User.js model file....
// 			or if it should belong in Express API's routes.js
User.findAll = function(req, res) {
	User.find({}, function(err, results){
		if(err) res.send(err);
		var sanitizedResults = [];
		for(var user in results) {
			var newUser = User(results[user]);
			// user_info to hide the password
			sanitizedResults.push(newUser.user_info);
		}
		return res.json(sanitizedResults);
	});
};
User.findByUsername = function(req, res) {
	console.log('User.findByUsername.req.body:',req.body); // DEBUG
	var username = req.params.username;
	User.findOne({'username':username}, function(err, result){
		if(err) res.send(err);
		var newUser = new User(result);
		return res.json(newUser.user_info);
	});
};
User.add = function(req, res) {
	console.log('User.add reached!');
	User.create(req.body, function(err, user){
		console.log('User.add.User.Create.req.body: ', req.body);
		if(err) return console.log(err);
		return res.send(user);
	});
};
User.update = function(req, res) {
	var username = req.params.username;
	var updates = req.body;
	User.update({'username':username}, updates, function(err, usersAffected){
		if(err) return console.log(err);
		console.log('Users updated(count): ', usersAffected);
		res.send(202); // Why status here and not the others? Is 200 implicitly returned on others?
	});
};
User.delete = function(req, res) {
	var username = req.params.username;
	User.remove({'username':username}, function(result){
		//return res.json(result);
		return res.send(result);
	});
};

module.exports = User;
